Building
========

    npm install
    npm run build

There is also ':watch' version of build.

    npm run build:watch

Demo
====

To serve built page, with autoreload, run

    npm run demo

Testing
=======

    npm run lint
    npm run test
    npm run check # equal to 'lint' and 'test'

These commands have ':watch' versions, which will rerun tasks when files are changed.

    npm run lint:watch
    npm run test:watch
    npm run check:watch
