describe('constants', function () {
    'use strict';

    var constants = require('app/scripts/constants.js');

    it ('contains 44 in text field', function () {
        expect(constants.text).toContain('44');
    });
});
