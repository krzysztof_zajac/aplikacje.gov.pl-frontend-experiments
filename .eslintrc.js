module.exports = {
    'env': {
        node: true,
        es6: true,
    },
    'extends': 'eslint:recommended',
    'rules': {
        'indent': ['error', 4],
        'linebreak-style': ['error', 'unix'],
        'quotes': ['error', 'single'],
        'semi': ['error', 'always'], // semicolon
        'comma-dangle': ['error', 'always-multiline'],
        'quote-props': ['error', 'consistent-as-needed', {keywords: true}],
        'func-style': ['error', 'expression'],
    },
};
