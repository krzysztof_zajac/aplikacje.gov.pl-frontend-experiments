let constants = require('./constants.js');
let template = require('app/templates/index.hbs');

module.exports = function (color, link) {
    document.write(template({
        color: color,
        link: link,
        constants: constants,
    }));
};
