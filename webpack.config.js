let path = require('path');
let webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
    entry: {
        index_pink: './src/scripts/index_pink.js',
        index_blue: './src/scripts/index_blue.js',
    },
    resolve: {
        alias: {
            app: path.resolve('src'),
        },
    },
    module: {
        loaders: [
            {
                test: /\.scss$/,
                loaders: ['style-loader', 'css-loader', 'sass-loader'],
            },
            {test: /\.hbs$/, loader: 'handlebars-loader'},
        ],
    },
    output: {
        path: 'dist',
        filename: '[name].js',
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin('common.js'),
        new HtmlWebpackPlugin({
            filename: 'index_pink.html',
            chunks: ['common.js', 'index_pink'],
        }),
        new HtmlWebpackPlugin({
            filename: 'index_blue.html',
            chunks: ['common.js', 'index_blue'],
        }),
    ],
};
